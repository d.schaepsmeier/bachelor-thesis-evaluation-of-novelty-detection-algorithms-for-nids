# Bachelor Thesis - Evaluation of Novelty Detection Algorithms for NIDS
This is the accompanying git repository for the bachelor thesis _Evaluation of Novelty Detection Algorithms for Network Intrusion Detection Systems_.

All Jupyter Notebooks created for the experiment are in the root directory with numbering for the respective chapter in the thesis. PDF versions of the Jupyter Notebook are under the `jupyter-pdfs` directory. All used Python Libraries can be found in the `requirements.txt`. 

All used original datasets are in `original_data`, while the preprocessed version can be found in `data`. The dimensionality-reduced datasets reside in `data_visualization`. All created images can be found in the `evaluation` directory. 
